﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.IO;

namespace LinqTest
{
    class Program
    {
        static void Main(string[] args)
        {
            string jsonInput = File.ReadAllText("../../data.js");
            JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();
            Order[] orders = jsonSerializer.Deserialize<Order[]>(jsonInput);

            Console.WriteLine(" Total orders {0} ", orders.Count());
            var result = orders
                            .GroupBy(c => c, c => c, new OrderComparer())
                            .OrderByDescending(c => c.Count())
                            .Take(20)
                            .Select(c => new { Toppings = c.Key.toppings, Orders = c.Count() });
            foreach (var e in result)
            {
                Console.WriteLine(" Orders {0,-6} with toppings: {1}", e.Orders, string.Join(", ", e.Toppings));

            }
            Console.WriteLine();
            Console.ReadKey();
        }
    }


    public class Order
    {
        public string[] toppings { get; set; }
    }

    public class OrderComparer : IEqualityComparer<Order>
    {

        public bool Equals(Order o1, Order o2)
        {
            if (Object.ReferenceEquals(o1.toppings, o2.toppings)) return true;
            if (Object.ReferenceEquals(o1.toppings, null) || Object.ReferenceEquals(o2.toppings, null))
                return false;

            int count = o2.toppings.Count();
            if (count == o1.toppings.Count() && count == o2.toppings.Intersect(o1.toppings).Count())
                return true;

            return false;
        }

        public int GetHashCode(Order o)
        {
            if (Object.ReferenceEquals(o.toppings, null)) return 0;

            int hashcode = 0;
            foreach (var e in o.toppings)
            {
                hashcode ^= e == null ? 0 : e.GetHashCode();
            }
            return hashcode;
        }
    }
}
