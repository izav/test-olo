# Code Test: #

http://olo.applytojob.com/apply/UfQHrv/Full-Stack-Software-Engineer#




**Finding Popular Build-Your-Own Pizzas**

A pizza chain wants to know which topping combinations
are most popular for Build Your Own pizzas.

Given the sample of orders at http://files.olo.com/pizzas.json,


```javascript
[
 {
   "toppings": [
     "mozzarella cheese"
    ]
  },
  {
   "toppings": [
     "pepperoni",
     "bacon",
     "diced tomatoes",
     "mushrooms",
     "chicken"
    ]
  },
  {
   "toppings": [
     "beef",
     "bacon",
     "pineapple"
    ]
  }

```



write an application (in C# or F#) to output the top 20 most 
frequently ordered pizza configurations, listing the toppings 
for each along with the number of times that pizza configuration 
has been ordered.

----------------------------------

# Result: #

![Test_Result.jpg](https://bitbucket.org/repo/Mrg5kX8/images/16718668-Test_Result.jpg)